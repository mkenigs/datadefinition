#!/usr/bin/env python3
# Copyright (c) 2018 - 2020 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Pipeline trigger variables definition."""
import os

from rcdefinition.rc_data import DefinitionBase


class TriggerVariables(DefinitionBase):
    # pylint: disable=R0902
    """Serialization class for trigger variables."""

    # Projects should always pass data by passing this object, or
    # representation created by this object's serialize/deserialize
    # methods. This ensures all necessary data is well-defined and present
    # during __init__. If something is missing and is required, __init__
    # must fail.

    # CI/Testing variables

    # https://gitlab.com/cki-project/cki-lib/-/archive/SHA/cki-lib-SHA.tar.gz
    # an url in this format will force the CI to install a different cki-lib
    # in prepare stage
    cki_lib_targz_url: str = None
    debug_email: str = None  # affects email / reporting, forces single address
    skip_beaker: str = None

    # All-purpose email & reporting variables
    mail_add_maintainers_to: str = None
    mail_to_task_owner: str = None
    mail_bcc: str = None
    mail_cc: str = None
    mail_from: str = None
    mail_to: str = None
    manual_review_mail_to: str = None
    report_template: str = None
    report_types: str = None  # to be removed: legacy pipelines only
    require_manual_review: str = None
    subject: str = None  # used for stable queue trigger as well
    send_report_to_upstream: str = None
    send_report_on_success: str = None

    # General CI variables
    cki_project: str = None
    cki_pipeline_id: str = None
    cki_pipeline_branch: str = None
    cki_pipeline_type: str = None
    name: str = None  # trigger name
    original_pipeline: str = None  # used for UMB messaging
    original_pipeline_id: int = None  # used for UMB messaging
    result_pipe: str = None  # used for UMB messaging
    retrigger: str = None

    # UMB messaging CI variables
    send_ready_for_test_pre: str = None
    send_ready_for_test_post: str = None

    # Gitlab CI variables
    pipeline_definition_branch_override: str = None
    pipeline_definition_repository_override: str = None
    status: str = None

    # Kernel artifacts variables: patch and patchwork related variables
    cover_letter: str = None
    date: str = None  # to be removed: legacy pipelines only
    event_id: int = None
    last_patch_id: str = None
    message_id: str = None
    patch_urls: str = None  # used for stable queue trigger as well
    patchwork_url: str = None
    patchwork_project: str = None
    submitter: str = None
    skipped_series: str = None

    #  Kernel artifacts variables: general-purpose variables
    commit_hash: str = None
    title: str = None

    # Kernel artifacts variables: Brew trigger variables
    data: str = None
    owner: str = None
    package_name: str = None
    rpm_release: str = None
    team_email: str = None
    team_name: str = None

    # Kernel artifacts variables: baseline trigger/stable queue variables
    branch: str = None
    git_url: str = None
    make_target: str = None

    # Kernel artifacts variables: stable queue
    queue_commit_hash: str = None
    queue_url: str = None

    # Most likely legacy variables
    merge_branch: str = None
    merge_tree: str = None

    # Space separated list of architectures to run the pipeline for, in RPM architecture format.
    # Required in every pipeline.
    architectures: str = None
    # `public` if the pipeline artifacts should be published on a public facing storage, `private`
    # otherwise. Defaults to `private`.
    artifacts_visibility: str = None
    # `True` if any kernel selftests should be built, `False` otherwise. Defaults to `False`.
    # Will cause `make kselftest-merge` configuration target to be called. Only applicable for
    # kernels built as tarballs.
    build_selftests: str = None
    # Container image used for kernel building. Defaults to `registry.gitlab.com/cki-project/
    # containers/builder-fedora`.
    builder_image: str = None
    # URL to archive to install by pip to provide cki-tools for a given pipeline.
    cki_tools_pip_url: str = None
    # `make` configuration target to use on top of Fedora configuration files. Defaults to
    # `olddefconfig`. Only applicable for kernels built as tarballs.
    config_target: str = None
    # Time of revision discovery to track for KCIDB. Optional. **NOTE** may be removed.
    discovery_time: str = None
    # `upstream` or `internal`. Currently only used to skip lint step for upstream kernels.
    # **NOTE** may be removed in the future.
    kernel_type: str = None
    # Value to pass to [kpet tree selection script] to pick appropriate test definitions.
    kpet_tree_family: str = None
    # Test set(s) to run for the pipeline, in the [kpet generate] `-s` option syntax. By default,
    # all sets are included for patch runs (and tests are picked via patch analysis) and official
    # kernel builds are limited to sanity `kt0` testing. Also check out documentation for targeted
    # testing for [builds] on how to limit test sets for a single build.
    test_set: str = None
    # Filename of the [pipeline YAML tree] used. Not used in the pipeline, but (re)triggers can
    # utilize this information to automatically create `.gitlab-ci.yml` for nonexistent branches
    # if needed.
    tree_yaml_name: str = None

    def pipeline_vars_from_env(self):
        # pylint: disable=no-member
        """Return all trigger variables as dict."""
        for attr in self.__annotations__:
            value = os.environ.get(attr, None)
            if value is not None:
                setattr(self, attr, value)

        return self.to_mapping()

    def print_var_names(self):
        # pylint: disable=no-member
        """Print names of trigger variables."""
        print(' '.join(self.__annotations__))
