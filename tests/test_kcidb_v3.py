# Copyright (c) 2020 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Gitlab api interaction tests."""
import os
import unittest
from unittest import mock

from kcidb_wrap import v3
from kcidb_wrap.v3 import is_empty


class TestKCIDBWrap(unittest.TestCase):
    """Test KCIDBObject class."""

    def setUp(self) -> None:
        self.raw_revision = {'id': 'ab1164e86e1898e3df162a48d7ab170aa396872f',
                             'origin': 'redhat',
                             'tree_name': 'rhel', 'git_repository_url': 'https://repo',
                             'git_commit_hash': 'deadbeefdeadbeefdeadbeefdeadbeefdeadbeef',
                             'git_commit_name': 'kernel-4.18.0-0.el7',
                             'git_repository_branch': 'rhel',
                             'description': '[patch] [patch]',
                             'publishing_time': '2000-12-02T10:00:00.000000Z',
                             'discovery_time': '2000-12-02T10:00:00.000000Z',
                             'contacts': [], 'valid': True, 'misc': {'pipeline_id': '1234'}}
        self.revision = v3.Revision(self.raw_revision)

        self.raw_build = {'revision_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                          'id': 'redhat:1',
                          'origin': 'redhat',
                          'description': 'data data',
                          'start_time': '2019-11-14T00:07:00Z',
                          'duration': 321,
                          'architecture': 'x86_64',
                          'command': 'make bzImage',
                          'compiler': 'foo 1.2',
                          'input_files': [],
                          'output_files': [
                              {'name': 'kernel.tar.gz',
                               'url': 'http://s3.server/kernel.tar.gz'}],
                          'config_name': 'fedora',
                          'config_url': 'http://s3.server/kernel.config',
                          'log_url': 'http://s3.server/testfile.log',

                          'misc': {'job_id': 3, 'pipeline_id': 3},
                          'valid': True}
        self.build = v3.Build(self.raw_build)

        self.raw_test_job_part = {"id": 1036855, "name": "test x86_64",
                                  "stage": "test",
                                  "started_at": "2000-12-02T10:00:00.000000Z",
                                  "created_at": "2000-12-02T10:00:00.000000Z",
                                  "finished_at": "2000-12-02T11:00:00.000000Z",
                                  "duration": 3600,
                                  "tag": "tag",
                                  "test_hash": "aaaaffff",
                                  "commit_message_title": None,
                                  "kernel_version": None}
        self.raw_test_pipeline_part = {"id": 618657, "variables": {},
                                       "started_at": "2000-12-02T10:00:00.000000Z",
                                       "created_at": "2000-12-02T10:00:00.000000Z",
                                       "finished_at": "2000-12-02T10:00:00.000000Z",
                                       "duration": 3600,
                                       "ref": "rhel-c6cbbcb6-1a70-4509-90e2-2949d9b284a3",
                                       "sha": "deadbeefdeadbeefdeadbeefdeadbeefdeadbeef",
                                       "project":
                                           {"id": 2,
                                            'instance_url': 'https://host.name.com',
                                            "path_with_namespace": "cki-project/cki-pipeline"}
                                       }
        self.raw_test = {"build_id": "redhat:1234", "id": "redhat:3",
                         "environment": {"description": "hostname"},
                         "description": "Boot test", "waived": False,
                         "start_time": "2000-12-02T10:00:00.000000Z",
                         "duration": 343,
                         "misc": {"debug": False, "targeted": False,
                                  "fetch_url": "https://fetch",
                                  "beaker":
                                      {"task_id": 3, "recipe_id": 123456,
                                       "finish_time": "2000-12-02T10:00:00.000000Z"},
                                  "job": self.raw_test_job_part,
                                  "pipeline": self.raw_test_pipeline_part},
                         "status": "PASS", "path": "boot", "origin": "redhat"}
        self.test = v3.Test(self.raw_test)

    def test_is_empty(self):
        """Test is_empty method."""
        test_cases = [
            (None, True),
            ('', False),
            ('something', False),
            ([], True),
            ([''], False),
            ({}, True),
            ({'a': 'b'}, False),
            (False, False),
        ]

        for case, expected in test_cases:
            self.assertEqual(expected, is_empty(
                case), (case, expected))

    def test_craft_build(self):
        """Ensure crafting and serialization works for build object."""
        self.assertTrue(self.build.is_valid())

    def test_craft_revision(self):
        """Ensure crafting and serialization works for revision object."""
        self.assertTrue(self.revision.is_valid())

    def test_craft_test(self):
        """Ensure crafting and serialization works for test object."""
        self.assertTrue(self.test.is_valid())

    def test_job_data(self):
        """Ensure feeding job_data method with raw data will produce expected result."""
        job_part = self.raw_test_job_part

        # This is env that will make .pipeline_data call produce expected data.
        env = {'CI_JOB_ID': '1036855',  'CI_JOB_NAME': 'test x86_64', 'CI_JOB_STAGE': 'test'}

        with mock.patch.dict(os.environ, env):
            result = v3.KCIDBObject.job_data('tag', self.raw_test_job_part['started_at'],
                                             self.raw_test_job_part['finished_at'],
                                             job_part['test_hash'], job_part['commit_message_title'],
                                             job_part['kernel_version'])
            self.assertEqual(self.raw_test_job_part, result)

    def test_pipeline_data(self):
        """Ensure feeding pipeline_data method with raw data will produce expected result."""

        # This is env that will make .pipeline_data call produce expected data.
        env = {'CI_PIPELINE_ID': '618657', 'CI_PROJECT_ID': '2',
               'CI_COMMIT_REF_NAME': 'rhel-c6cbbcb6-1a70-4509-90e2-2949d9b284a3',
               'CI_COMMIT_SHA': 'deadbeefdeadbeefdeadbeefdeadbeefdeadbeef',
               'CI_PROJECT_PATH': 'cki-project/cki-pipeline',
               'CI_SERVER_URL': 'https://host.name.com'}
        with mock.patch.dict(os.environ, env):
            result_pipe_data = v3.KCIDBObject.pipeline_data({}, self.raw_test_pipeline_part['started_at'],
                                                            self.raw_test_pipeline_part['finished_at'],
                                                            self.raw_test_pipeline_part['duration'])
            self.assertEqual(self.raw_test_pipeline_part, result_pipe_data)

    def test_render(self):
        """Ensure render works."""
        expected = {"build_id": "redhat:1234", "id": "redhat:3", "origin": "redhat"}

        input_data = expected.copy()
        # The field below will be removed by render().
        input_data['status'] = None

        self.assertEqual(expected, v3.Test(input_data).render())

    def test_merge_kcidb_data(self):
        kcidb_data = v3.craft_kcidb_data([self.revision], [self.build], [self.test])
        merged_data = v3.merge_kcidb_data([kcidb_data, kcidb_data])
        self.assertEqual(len(merged_data['revisions']), 2)
        self.assertEqual(len(merged_data['builds']), 2)
        self.assertEqual(len(merged_data['tests']), 2)
