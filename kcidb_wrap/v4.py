#!/usr/bin/env python3
"""Craft kcidb data."""
import os

from dateutil.parser import parse as date_parse
from kcidb_io.schema import is_valid
from kcidb_io.schema.v4 import JSON
from kcidb_io.schema.v4 import JSON_BUILD
from kcidb_io.schema.v4 import JSON_CHECKOUT
from kcidb_io.schema.v4 import JSON_TEST
from kcidb_io.schema.v4 import JSON_VERSION_MAJOR
from kcidb_io.schema.v4 import JSON_VERSION_MINOR

from rcdefinition.rc_data import DefinitionBase
from rcdefinition.rc_data import _NO_DEFAULT


def craft_kcidb_data(checkouts, builds, tests):
    """Craft kcidb schema from internal data."""
    craft = {"version": dict(major=JSON_VERSION_MAJOR,
                             minor=JSON_VERSION_MINOR),
             "checkouts": checkouts, "builds": builds, "tests": tests}
    return craft


def is_empty(value):
    """Check if value is not empty."""
    return (
        value is None or
        value == [] or
        value == {}
    )


class KCIDBObject(DefinitionBase):
    """KCIDB v4 schema (de)serialization."""

    # dict object that describes metadata
    meta = JSON
    # type conversion table
    _conv_table = {'object': dict,
                   'string': str,
                   'number': int,
                   'array': list,
                   'boolean': bool}

    def __init__(self, dict_data=None):
        """Create an object."""
        self.from_meta(self.meta)
        super().__init__(dict_data)

    def render(self):
        """Return object data removing empty keys."""
        return {
            key: value for key, value in self.to_mapping().items() if not is_empty(value)
        }

    def from_meta(self, schema):
        """Modify annotations to contain keys/types according to metadata."""
        self.__annotations__ = {}
        properties = schema['properties']
        required = schema['required']

        for key, value in properties.items():
            actual_type = KCIDBObject._conv_table[value['type']]
            self.__annotations__[key] = actual_type
            if key in required:
                setattr(self, key, _NO_DEFAULT)

    @classmethod
    def pipeline_data(cls, variables, publishing_time, finished_at, duration):
        """Craft info about the pipeline that run."""
        return {
            'id': int(os.environ['CI_PIPELINE_ID']),
            'variables': variables,
            'started_at': publishing_time,
            'created_at': publishing_time,
            'finished_at': finished_at,
            'duration': duration,
            'ref': os.environ['CI_COMMIT_REF_NAME'],
            'sha': os.environ['CI_COMMIT_SHA'],
            'project': {
                'id': int(os.environ['CI_PROJECT_ID']),
                'path_with_namespace': os.environ['CI_PROJECT_PATH'],
                'instance_url': os.environ['CI_SERVER_URL']
            }
        }

    @classmethod
    def job_data(cls, tag, started_at, finished_at, test_hash, commit_message_title,
                 kernel_version):
        # pylint: disable=too-many-arguments
        """Craft info about the job that ran."""
        duration = int((date_parse(finished_at) - date_parse(started_at)).total_seconds()) \
            if finished_at is not None and started_at is not None else None
        return {
            'id': int(os.environ['CI_JOB_ID']),
            'name': os.environ['CI_JOB_NAME'],
            'stage': os.environ['CI_JOB_STAGE'],
            'started_at': started_at,
            'created_at': started_at,
            'finished_at': finished_at,
            'duration': duration,
            'test_hash': test_hash,
            'tag': str(tag) if tag else None,
            'commit_message_title': commit_message_title,
            'kernel_version': kernel_version,
        }


class Checkout(KCIDBObject):
    """KCIDB v4 Checkout object."""

    meta = JSON_CHECKOUT
    # str, name that DW and kcidb uses to identify the object
    resource_name = meta['title']

    def is_valid(self):
        """Check if the data in this object is valid."""
        return is_valid(craft_kcidb_data([self.to_mapping()], [], []))


class Build(KCIDBObject):
    """KCIDB v4 Build object."""

    meta = JSON_BUILD
    # str, name that DW and kcidb uses to identify the object
    resource_name = meta['title']

    def is_valid(self):
        """Check if the data in this object is valid."""
        return is_valid(craft_kcidb_data([], [self.to_mapping()], []))


class Test(KCIDBObject):
    """KCIDB v4 Test object."""

    meta = JSON_TEST
    # str, name that DW and kcidb uses to identify the object
    resource_name = meta['title']

    def is_valid(self):
        """Check if the data in this object is valid."""
        return is_valid(craft_kcidb_data([], [], [self.to_mapping()]))


class KSelfTest(Test):
    """KCIDB v4 Test object for kselftest."""


class UMBTest(Test):
    """KCIDB v4 Test object for UMB tests."""
